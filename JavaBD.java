import java.sql.*;
public class JavaBD {
	public static void main(String[]args) throws Exception {
		Class.forName("com.mysql.jdbc.Driver");
        Connection connect = DriverManager.getConnection(
			"jdbc:mysql://localhost/labase&user=ann&password=123");

		PreparedStatement ps1 = connect
			.prepareStatement("CREATE TABLE t1(texte VARCHAR(100));");
        ps1.executeUpdate();

		PreparedStatement ps2 = connect
			.prepareStatement("insert into t1 VALUES(?)");
		ps2.setString(1, "test");
        ps2.executeUpdate();
		connect.close();
	}
}
